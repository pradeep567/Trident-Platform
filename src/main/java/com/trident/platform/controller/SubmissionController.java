package com.trident.platform.controller;

import io.swagger.annotations.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/submission")
@Api(tags = "submission")
public class SubmissionController {

    @PostMapping("/")
    @ApiOperation(value = "${SubmissionController.submission}")
    @PreAuthorize("hasRole('ROLE_CLIENT')")
    @ApiResponses(value = {//
            @ApiResponse(code = 400, message = "Bad Request")})
    public String submission(@ApiParam("Username") @RequestParam String username) {
        return "Submission Request Recieved";
    }

    @GetMapping("/health")
    public String heathCheck() {
        return "OK";
    }
}
